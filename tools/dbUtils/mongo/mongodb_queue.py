# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from pymongo import MongoClient, errors
from bson.objectid import ObjectId
class MogoQueue():

    OUTSTANDING = 1 ##初始状态
    PROCESSING = 2 ##正在下载状态
    COMPLETE = 3 ##下载完成状态

    def __init__(self, db, collection, timeout=30000):##初始mongodb连接
        self.client = MongoClient()
        self.Client = self.client[db]
        self.db = self.Client[collection]
        self.timeout = timeout

    def __bool__(self):
        """
        这个函数，我的理解是如果下面的表达为真，则整个类为真
        至于有什么用，后面我会注明的（如果我的理解有误，请指点出来谢谢，我也是Python新手）
        $ne的意思是不匹配
        """
        record = self.db.find_one(
            {'state': {'$ne': self.COMPLETE}}
        )
        return True if record else False

    def push(self, data): ##这个函数用来添加新的URL进队列
        try:
            self.db.insert(data)
            print('插入队列成功')
        except errors.DuplicateKeyError as e:  ##报错则代表已经存在于队列之中了
            print('已经存在于队列中了')
            pass

    def pop(self):
        """
        这个函数会查询队列中的所有状态为OUTSTANDING的值，
        更改状态，（query后面是查询）（update后面是更新）
        并返回_id（就是我们的ＵＲＬ），MongDB好使吧，^_^
        如果没有OUTSTANDING的值则调用repair()函数重置所有超时的状态为OUTSTANDING，
        $set是设置的意思，和MySQL的set语法一个意思
        """
        record = self.db.find_and_modify(
            query={'state': self.OUTSTANDING},
            update={'$set': {'state': self.PROCESSING, 'timestamp': datetime.now()}}
        )
        if record:
            return record['_id']
        else:
            self.repair()
            raise KeyError

    def pop_id(self, id,key_lists):
        conditions={"_id":-1}
        for key in key_lists:conditions[key]=1
        record = self.db.find_one({'_id': id},conditions)
        return record

    def peek(self):
        """这个函数是取出状态为 OUTSTANDING的文档并返回_id(URL)"""
        record = self.db.find_one({'state': self.OUTSTANDING})
        if record:
            return record['_id']

    def complete(self, _id):
        """这个函数是更新已完成的URL完成"""
        self.db.update({'_id': _id}, {'$set': {'state': self.COMPLETE}})

    def repair(self):
        '''
        """这个函数是重置状态$lt是比较"""
        record = self.db.find_and_modify(
           query={
               'timestamp': {'$lt': datetime.now() - timedelta(seconds=self.timeout)},
               'state': {'$ne': self.COMPLETE}
           },
            update={'$set': {'state': self.OUTSTANDING}}
        )
        if record:
            print('重置image状态', record['_id'])
        '''
        pass
    @staticmethod
    def get_ObjectId(id=None):
        if id is not None:
            if not isinstance(id, ObjectId):id = ObjectId(id)
        else:
            id=ObjectId()
        return id

    def clear(self):
        """这个函数只有第一次才调用、后续不要调用、因为这是删库啊！"""
        self.db.drop()