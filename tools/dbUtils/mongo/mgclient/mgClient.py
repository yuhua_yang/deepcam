#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2017/8/28 9:04
@Author  : fuyu
@File    : Databases.py
@Software: IntelliJ IDEA
@Desc    : Function description
'''
#     '''
#     db.collection.update( criteria, objNew, upsert, multi )
#     criteria : update的查询条件，类似sql update查询内where后面的
#     objNew   : update的对象和一些更新的操作符（如$,$inc...）等，也可以理解为sql update查询内set后面的
#     upsert   : 这个参数的意思是，如果不存在update的记录，是否插入objNew,true为插入，默认是false，不插入。
#     multi    : mongodb默认是false,只更新找到的第一条记录，如果这个参数为true,就把按条件查出来多条记录全部更新。
#     :param _id:
#     :param updateData:
#     :param tableName:
#     :param upsert:
#     :param multi:
#     :return:
#     '''


import sys

reload(sys)
sys.setdefaultencoding('utf-8')

from pymongo import MongoClient
from bson.objectid import ObjectId
import json

def id_to_ObjectId_list(data):
    return map(lambda id : ObjectId(id), [y for y in data])
def get_ObjectId(id=None):
    if id is not None:
        if not isinstance(id, ObjectId):id = ObjectId(id)
    else:
        id=ObjectId()
    return id


class ObjectIdEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return obj.__str__()
        return json.JSONEncoder.default(self, obj)

class MGClient(object):
    def __init__(self,dbName="deepcam",host='localhost'):
        #创建连接
        self.conn = MongoClient(host, 27017)
        self.dbName=dbName
        self.db = self.conn[dbName]

    def getTable(self,tableName):
        table=self.db[tableName]
        return table


    def close(self):
        self.conn.close()
