#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2017/8/28 9:04
@Author  : fuyu
@File    : __init__.py.py
@Software: IntelliJ IDEA
@Desc    : Function description
'''
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
from mgclient.mgClient import *
from mongowrapper.MongoWrapper import MongoWrapper
