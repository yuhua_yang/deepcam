# coding=utf-8
import Image
import os
import shutil
import datetime
from ..path import mkdir
def getImageSize(infile):
    im = Image.open(infile)
    (width, height)=im.size
    return width,height

class Graphics:
    def __init__(self, infile, dir):
        self.infile = infile
        self.im = Image.open(infile)
        self.size = self.im.size
        self.dir = dir
        mkdir(dir)

    def fixed_size(self, width, height):
        filepath = self.getfilepath()
        """按照固定尺寸处理图片"""
        out = self.im.resize((width, height), Image.ANTIALIAS)
        out.save(filepath)

    def resize_by_width(self, w_divide_h):
        filepath = self.getfilepath()
        """按照宽度进行所需比例缩放"""
        (x, y) = self.im.size
        x_s = x
        y_s = x / w_divide_h
        out = self.im.resize((x_s, y_s), Image.ANTIALIAS)
        out.save(filepath)
        return filepath

    def resize_by_height(self, w_divide_h):
        filepath = self.getfilepath()
        """按照高度进行所需比例缩放"""
        (x, y) = self.im.size
        x_s = y * w_divide_h
        y_s = y
        out = self.im.resize((x_s, y_s), Image.ANTIALIAS)
        out.save(filepath)
        return filepath

    def resize_by_size(self, size):
        filepath = self.getfilepath()
        """按照生成图片文件大小进行处理(单位KB)"""
        size *= 1024
        size_tmp = os.path.getsize(self.infile)
        q = 100
        while size_tmp > size and q > 0:
            print q
            out = self.im.resize(self.im.size, Image.ANTIALIAS)
            out.save(filepath, quality=q)
            size_tmp = os.path.getsize(self.getfilepath)
            q -= 5
        if q == 100:
            shutil.copy(self.infile, filepath)
        return filepath

    def cut_by_ratio(self, width, height):
        filepath = self.getfilepath()
        """按照图片长宽比进行分割"""
        width = float(width)
        height = float(height)
        (x, y) = self.im.size
        if width > height:
            region = (0, int((y - (y * (height / width))) / 2), x, int((y + (y * (height / width))) / 2))
        elif width < height:
            region = (int((x - (x * (width / height))) / 2), 0, int((x + (x * (width / height))) / 2), y)
        else:
            region = (0, 0, x, y)

            # 裁切图片
        crop_img = self.im.crop(region)
        # 保存裁切后的图片
        crop_img.save(filepath)
        return filepath

    def cut_by_quarter(self, type=1):
        filepath = self.getfilepath()
        """按照图片长宽比进行分割"""
        (x, y) = self.im.size
        qx = x / 2
        qy = y / 2
        region = (0, 0, x, y)
        if type == 1:
            region = (0, 0, qx, qy)
        elif type == 2:
            region = (qx, 0, x, qy)
        elif type == 3:
            region = (0, qy, qx, y)
        elif type == 4:
            region = (qx, qy, x, y)
        # 裁切图片
        crop_img = self.im.crop(region)
        # 保存裁切后的图片
        crop_img.save(filepath)
        return filepath

    # region=(2,6,140,340)  --*--(x1,y1,x2,y2)---*--
    def cutPicture(self, roi,range=0, filepath=None):
        if filepath is None:
            filepath = self.getfilepath()

        x=roi["x"]
        y=roi["y"]
        w=roi["w"]
        h=roi["h"]
        xexpansion=w*range*0.25
        yexpansion=h*range*0.25
        (x1,y1)=self.getPosition((x-xexpansion,y-yexpansion))
        (x2,y2)=self.getPosition((x+w+xexpansion,y+h+yexpansion))
        region=(x1,y1,x2,y2)
        # 裁切图片
        crop_img = self.im.crop(region)
        # 保存裁切后的图片
        crop_img.save(filepath)
        return filepath

    def getfilepath(self):
        return os.path.join(self.dir, "{}.jpg".format(datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")))


    def getPosition(self,position):
        (width, height)=self.size
        (x,y)=position
        x=int(x)
        y=int(y)
        if x<=0:x=0
        if x>=width:x=width
        if y<=0:y=0
        if y>=height:y=height
        return (x,y)
