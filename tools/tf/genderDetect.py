#!/usr/bin/env python
# -*- coding: utf-8 -*-
# /*************************************************************************
# deepCam CONFIDENTIAL

# [2016] - [2017] DeepCam, LLC and DeepCam
# All Rights Reserved.
# NOTICE:
# All information contained herein is, and remains the property of DeepCam LLC.
# The intellectual and technical concepts contained herein are proprietary to DeepCam
# and may be covered by U.S. and Foreign Patents,patents in process, and are protected by
# trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# DeepCam, LLC.
#
# Written: FN LN Date
# Updated:
# /


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import tensorflow as tf

    #load the tensorflow pb file
def load_graph(model_file):
    graph = tf.Graph()
    graph_def = tf.GraphDef()

    with open(model_file, "rb") as f:
        graph_def.ParseFromString(f.read())
    with graph.as_default():
        tf.import_graph_def(graph_def)

    return graph

    #read tensor from your image file
def read_tensor_from_image_file(file_name, input_height=48, input_width=48,
                                input_mean=0, input_std=255):
    input_name = "file_reader"
    output_name = "normalized"
    file_reader = tf.read_file(file_name, input_name)

    if file_name.endswith(".png"):
        image_reader = tf.image.decode_png(file_reader, channels=1,
                                           name='png_reader')
    elif file_name.endswith(".gif"):
        image_reader = tf.squeeze(tf.image.decode_gif(file_reader,
                                                      name='gif_reader'))
    elif file_name.endswith(".bmp"):
        image_reader = tf.image.decode_bmp(file_reader, name='bmp_reader')
   
    else:
        image_reader = tf.image.decode_jpeg(file_reader, channels=1,
                                            name='jpeg_reader')                # this can put to first
    float_caster = tf.cast(image_reader, tf.float32)
    dims_expander = tf.expand_dims(float_caster, 0)
    resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
    normalized = tf.divide(tf.subtract(resized, [input_mean]), [input_std])

    tf_config = tf.ConfigProto()  
    tf_config.gpu_options.allow_growth = True           # limit the GPU memory to auto growth
    sess = tf.Session(config=tf_config)

    return sess.run(normalized)

def detect(flow,tensor):

    labels = {0:"female",1:"male"}
    input_layer = 'conv2d_1_input'
    output_layer = 'output_node0'
        
    input_name = "import/" + input_layer
    output_name = "import/" + output_layer
    input_operation = flow.get_operation_by_name(input_name)
    output_operation = flow.get_operation_by_name(output_name)
    
    tf_config = tf.ConfigProto()
    tf_config.gpu_options.allow_growth = True

    # compile the model
    with tf.Session(graph=flow, config=tf_config) as sess:
        results = sess.run(output_operation.outputs[0],
                           {input_operation.outputs[0]:tensor})
    results = np.squeeze(results)
        # write out the final test lable and probability
    idxs = np.argsort(results)[::-1]
    binaResult = labels.get(idxs[0])

    return binaResult, results[idxs[0]]*100


