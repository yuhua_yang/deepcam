# !/usr/bin/env python
# coding=utf-8

import tensorflow as tf


class ObjectClassify:
    def __init__(self):
        pass

    @staticmethod
    def get_label_lines(GFile,FastGFile):
        label_lines = [line.rstrip() for line
                       in tf.gfile.GFile("%s.txt"%GFile)]
        with tf.gfile.FastGFile("%s.pb"%FastGFile, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            _ = tf.import_graph_def(graph_def, name='')
        return label_lines

    def initSession(self):
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.05)
        sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
        self.softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
        self.sess = sess

    def detect(self, image_data, label_lines):
        predictions = self.sess.run(self.softmax_tensor, \
                                    {'DecodeJpeg/contents:0': image_data})
        top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
        # result = self.label_lines[1]
        for node_id in top_k:
            classify = label_lines[node_id]
            score = predictions[0][node_id]
            # print('%s (score = %.5f)' % (human_string, score))
            return classify, score
        return None, None
    @staticmethod
    def readImage(image_path):
        image_data = tf.gfile.FastGFile(image_path, 'rb').read()
        return image_data
