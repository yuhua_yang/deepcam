#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2017/7/24 14:32
@Author  : fuyu
@File    : cmuthread.py
@Software: IntelliJ IDEA
@Desc    : Function description
'''
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
'''
options对应的name:cmu_cut_options
    传入参数列表
host:服务器ip
port:cmu提取特征端口
cmuversion:cmu版本号 （1.3.6，1.3.7）
facedir:人脸存放位置
filedir:文件存放位置
htmldir:url的绝对目录 (/var/www/html)
others:roi位置（{x:40,y:56,w:400,h:400}）
# 请求报文
{"version":"1.3.7","cmdType":"image","inputPath":"./resources/images/face.jpg","outputPath":"/tmp/Derron","others":{}}
响应报文
{"result":"success","data":{"totalCount":"3","faces":[{"index":1,"name":"12424dsfsafafd82734.jpg","faceCount":1,"faceDatas":[{"index":1,"name":"张三"}]},{"index":2,"name":"242342dfssasfdasda.jpg","faceCount":2,"faceDatas":[{"index":2,"name":"王五"}]}]}}
目录结构
outputPath
├── faces    				(图片目录)
│   ├── face_1_1.jpg			(第1张图片的第1个人脸)
│   ├── face_2_1.jpg			(第2张图片的第1个人脸)
│   └── face_2_2.jpg			(第2张图片的第2个人脸)
└── features
├── feature_1_1.bin		(第1张图片的第1个人脸的特征值)
├── feature_2_1.bin		(第2张图片的第1个人脸的特征值)
└── feature_2_2.bin		(第2张图片的第2个人脸的特征值)
'''

import os
import json
import time
from tools.tool import *
from ..path import is_file_exists, mkdir
from ..socketUtils.tcpSocket.client import sendData
from ..featureUtils import toFeatures
import base64

class CmuInter:
    def __init__(self, option=None):
        if option is not None:
            self.option = option
        else:
            self.option = {
                "host": "127.0.0.1",
                "port": 5005,
                "version": "1.3.7",
                "others": {},
                "dirname": "cut"
            }

    '''
        :param data:{'inputPath':'./resources/images/face.jpg','others':{"faces": [{’y': 74, ’h': 103, ’w': 87, ’x': 0}]}}
        :param dirpath:文件的输出目录
        :param req:{"version":"1.3.7","cmdType":"image","inputPath":"./resources/images/face.jpg","outputPath":"/tmp/Derron","others":{}}
        :return:
        '''

    def cutImage(self, data, cmdType="image"):
        imagepath = data["inputPath"]
        outputPath = self.__getUniqueDirPath()
        if is_file_exists(imagepath):
            req = {"version": "1.3.7", "cmdType": cmdType, "inputPath": os.path.abspath(imagepath),
                   "outputPath": outputPath}
            if "others" in data:
                req["others"] = data["others"]
            response = self.__send_to__(req)
        else:
            response = exitFail("can't find file:{}".format(imagepath))
        response["outputPath"]=outputPath
        return response
    @staticmethod
    def load_feature(face):
        contentStr = base64.b64decode(face.pop("feature"))
        faceCount, features_ = toFeatures(len(contentStr), contentStr)
        return faceCount, features_


    def __send_to__(self, req):
        return sendData(json.dumps(req), self.option["host"], self.option["port"])

    def __getUniqueDirPath(self, rootdir="./data"):
        cutfacedir = os.path.join(rootdir, self.option["dirname"], str(int(round(time.time() * 1000))))
        return mkdir(cutfacedir)
