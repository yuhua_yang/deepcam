#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2017/8/30 17:29
@Author  : fuyu
@File    : tool.py
@Software: IntelliJ IDEA
@Desc    : Function description
'''
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import os
import json



def exitFail(strData=""):
    if not (isinstance(strData, dict) or isinstance(strData, str)):
        strData="{}".format(strData)

    response={
        "errorMessage" : strData,
        "result" : "error"
    }
    return response
def exitSuccess(str=""):
    response={
        "data" : str,
        "result" : "success"
    }
    return response

def getJsonVal(data, key, default=None):
    try:
        val = data[key]
    except:
        val = default
    return val

def getREQUEST(request):
    if request.method == 'POST':
        return request.POST
    elif request.method == 'GET':
        return request.GET

'''
获取request中的信息
'''
def getREQUESTData(data, key, default=None):
    if default is None:
        val=data.get(key)
        if val is None:
            raise  Exception("{} is not find".format(key))
    else:
        val=data.get(key,default)
    return val

'''
分页条件
'''
def getPageCondition(data):
    pageSize=int(data.get("pageSize",10))
    pageNumber=int(data.get("pageNumber",1))
    return pageNumber,pageSize

def gettargetpath(target="dbName",category="image"):
    return os.path.join(gettarget(target),category)
def gettarget(target="dbName"):
    return os.path.join(".","data","target",target)
def gettargetrootdir():
    return os.path.join(".","data","target")


def clearData(datas):
    if datas is not None:
        del datas[:]
        datas=None
        datas=[]
    else:
        datas=[]
    return datas

def loadCJson(str):
    try:
        str=str.decode('utf-8').replace("\n", "").replace(" ","")
    except:
        str=str
    try:
        jsonData=json.loads(str)
        return jsonData
    except:
        pass
    return None

