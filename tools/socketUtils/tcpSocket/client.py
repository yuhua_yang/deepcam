# encoding=utf-8
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import socket
import json
from tools.tool import *
class TcpClient:
    def __init__(self, HOST, PORT):
        self.BUFSIZ = 102400
        self.ADDR = (HOST, PORT)
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # socket.setdefaulttimeout(0.01)

    def conn(self):
        try:
            self.client.connect(self.ADDR)
        except:
            print "socket connect error"
            return False
        return True

    def send(self, url):
        self.client.send(url)
        data = self.client.recv(self.BUFSIZ)
        self.client.close()
        return data


def sendData(request, host, port):
    response=None
    try:
        client = TcpClient(host, port)
        if client.conn():
            response = json.loads(client.send(request))
        else:
            response=exitFail("socket connect error")
    except socket.timeout as e:
        response=exitFail("socket timout:{}".format(host))
    return response

