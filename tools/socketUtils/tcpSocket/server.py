# encoding=utf-8
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import fcntl
import socket
import struct
import traceback
from SocketServer import ThreadingTCPServer, BaseRequestHandler


def get_ip2(ifname="eth0"):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])


class MyBaseRequestHandlerr(BaseRequestHandler):
    def handle(self):
        client_ip = str(self.client_address[0])
        # print "client ip:" + client_ip
        self.execOpen()
        while True:
            try:
                data = self.request.recv(102400).strip()
                if not data:
                    break
                self.execMessage(data)
            except:
                traceback.print_exc()
                break

    def sendData(self, data):
        self.request.send(data)

    def execMessage(self, data):
        pass

    def execOpen(self):
        pass


def serveforever(RequestHandler, ip="127.0.0.1", port=8091):
    addr = (ip, port)
    server = ThreadingTCPServer(addr, RequestHandler)
    server.serve_forever()


if __name__ == "__main__":
    ip = get_ip2('eth0')
    port = 8091
    serveforever(MyBaseRequestHandlerr, ip, port)
