# encoding=utf-8
import commands

import json
import os
import base64
import threading
import requests

from collections import defaultdict
def lst_to_list(lst):
    # lst = [{'a': 123}, {'a': 456},{'b': 789}]
    # {'a': [123, 456]}, {'b': [789]}
    dic = {}
    for _ in lst:
        for k, v in _.items():
            dic.setdefault(k, []).append(v)
    return dic
    # return [{k:v} for k, v in dic.items()]


def compress_UUID():
    import uuid
    SAFEHASH = [x for x in "0123456789-abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ"]
    row = str(uuid.uuid4()).replace('-', '')
    safe_code = ''
    for i in xrange(10):
        enbin = "%012d" % int(bin(int(row[i * 3] + row[i * 3 + 1] + row[i * 3 + 2], 16))[2:], 10)
        safe_code += (SAFEHASH[int(enbin[0:6], 2)] + SAFEHASH[int(enbin[6:12], 2)])
    return safe_code


def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input


# tryEncode(responsedataStr)          # 默认用 UTF-8 进行编码
# tryEncode(responsedataStr, "ascii") # 尝试用 ASCII 进行编码
# tryEncode(responsedataStr, "GB2312")  # 尝试用 GB2312 进行编码
def tryEncode(s, encoding="utf-8"):
    try:
        return (json.loads(s.encode(encoding)))
    except UnicodeEncodeError as err:
        return (err)


def getCmd(cmd):
    status = None
    output = None
    try:
        (status, output) = commands.getstatusoutput(cmd)
    except Exception, e:
        print "cmd:{},{},{}".format(cmd, Exception, e)
    return status, output


def kill_process_by_name(name):
    cmd = "ps -ax | grep %s" % name
    f = os.popen(cmd)
    txt = f.readlines()
    if len(txt) == 0:
        print "no process \"%s\"!!" % name
        return
    else:
        for line in txt:
            colum = line.split()
            pid = colum[0]
            cmd = "kill -9 %d" % int(pid)
            rc = os.system(cmd)
    return


def base64_encode_array(a):
    response = None
    try:
        if a is not None:
            # return the base64 encoded array
            response = base64.b64encode(a)
    except Exception, e:
        print("base64_encode_array:{},{}".format(Exception, e))
    return response


def base64_decode_array(a):
    response = None
    try:
        if a is not None:
            # return the base64 encoded array
            response = base64.b64decode(a)
    except Exception, e:
        print("base64_decode_array:{},{}".format(Exception, e))
    return response


def startTread(method):
    # more thread for each request
    server_thread = threading.Thread(target=method)
    # Exit the server thread when the main thread terminates
    server_thread.daemon = True
    server_thread.start()
    return server_thread
