#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2017/8/9 13:33
@Author  : fuyu
@File    : VipClass.py
@Software: IntelliJ IDEA
@Desc    : Vip特征库
'''
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
from ..featureUtils import getfeature,getname
import os
from tools.path import is_file_exists, mvFileOrDir, mkdir
from tools.fileUtils.tar import make_targz

class Monitor(object):
    def checkt_files(self, names, imagespath, featurespath, anglecount=1):
        '''
        :param names: 人名列表
        :param features:特征值列表
        :param images:图片列表
        :return:
        '''
        names_count = len(names)
        images_count = len(imagespath)
        features_count = len(featurespath)
        if names_count > 0 and names_count == images_count and names_count == features_count:
            features = []
            for i in xrange(names_count):
                if is_file_exists(imagespath[i]) and is_file_exists(featurespath[i]):
                    _, feature = getfeature(featurespath[i])
                    if _ == anglecount:
                        features.extend(feature)
                    else:
                        return None
                else:
                    return None
            return features
        return None
    def move_file_to_rootdir(self, rootdir,names,imagespath):
        urls = []
        for i in xrange(len(names)):
            url = os.path.join(rootdir, "{}.jpg".format(names[i]))
            mvFileOrDir(imagespath[i], url)
            urls.append(url)
        return True, urls

    def clear(self):
        for key in self.__dict__.keys():
            del self.__dict__[key]







    def getFaceNames(self,filepath):
        return getname(filepath)

