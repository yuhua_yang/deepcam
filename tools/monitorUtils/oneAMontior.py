#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2017/9/23 9:08
@Author  : fuyu
@File    : featureUtils.py
@Software: IntelliJ IDEA
@Desc    : Function description
'''
import os

from monitor import Monitor
from tools.path import is_file_exists, mkdir, clearFileOrDir, write_content
from ..featureUtils import addDb, getname, toNpArray,getfeature
from ..tool import *
import time
'''
self.__option对应的name:vip_self.__option
  传入参数列表
vipdir:vip特征值位置
vipfacedir:vip人脸的位置
viptar:vip打包路径
tmptar:当vip个数为0时，提供一个默认的文件
tmpanglefile:angle.txt文件
'''


class OneAMontior(Monitor):
    def __init__(self):
        self.anglecount = 1
        self.range = 1.5
        self.__option = {
        }

    def check_root_dir(self):
        if "rootdir" not in self.__option:
            self.__option["rootdir"] = "./data"
            return False
        return True
    def get_face_path(self,name):
        return os.path.join(self.__option["facedir"], "{}.jpg".format(name))

    def get_face_dir(self):
        return self.__option["facedir"]
    def get_feature_dir(self):
        return self.__option["featuredir"]

    def get_root_dir(self):
        return self.__option["rootdir"]

    def get_face_names(self):
        return getname(self.__option["nameDbPath"])

    def initial_directory(self):
        self.check_root_dir()
        self.__initial_directory()
        mkdir(self.__option["rootdir"])
        mkdir(self.__option["facedir"])
        featureDbPath = self.__option["featureDbPath"]
        nameDbPath = self.__option["nameDbPath"]
        if not is_file_exists(featureDbPath):
            write_content("", featureDbPath)
        if not is_file_exists(nameDbPath):
            write_content("", nameDbPath)

    def __initial_directory(self):
        self.__option["featureDbPath"] = os.path.join(self.__option["rootdir"], "features.bin")
        self.__option["nameDbPath"] = os.path.join(self.__option["rootdir"], "names.txt")
        self.__option["facedir"] = os.path.join(self.__option["rootdir"], "image")
        self.__option["featuredir"] = os.path.join(self.__option["rootdir"], "feature")

    def initial_target_directory(self, dbName):
        self.__option["rootdir"] = gettarget(dbName)
        self.initial_directory()
        return self

    def initial_vip_directory(self):
        self.__option["rootdir"] = "./data/vipDB"
        self.initial_directory()
        return self

    def get_file_time(self):
        filepath=self.__option["nameDbPath"]
        if is_file_exists(filepath):
            return str(os.stat(filepath).st_mtime)
        return str(time.time())

    def getDbs(self):
        if is_file_exists(self.__option["nameDbPath"]) and is_file_exists(self.__option["featureDbPath"]):
            nc,names=getname(self.__option["nameDbPath"])
            fc,features=getfeature(self.__option["featureDbPath"])
            if nc==fc:
                return names,features
        return [],[]

    def clearFaceFeatures(self, isZero=True):
        clearFileOrDir(self.__option["rootdir"])
        if isZero:
            self.__initial_directory()

    def addFaceFeatures(self, names, features):
        features = toNpArray(features)
        shape = features.shape
        if shape[0] == len(names) and shape[-1] == 512:
            count = addDb(self.__option["nameDbPath"], names, self.__option["featureDbPath"], features)
            return True
        return False
    def replaceFaceFeatures(self, names, features):
        features = toNpArray(features)
        shape = features.shape
        if shape[0] == len(names) and shape[-1] == 512:
            clearFileOrDir(self.__option["nameDbPath"])
            clearFileOrDir(self.__option["featureDbPath"])
            count = addDb(self.__option["nameDbPath"], names, self.__option["featureDbPath"], features)
            return True
        return False

    def addFace(self, names, imagespath, featurespath):
        '''
        :param names: 人名列表
        :param features:特征值列表
        :param images:图片列表
        :return:
        '''
        features = self.checkt_files(names, imagespath, featurespath, self.anglecount)
        return self.addFeatures(names, imagespath, features)

    def addFeatures(self, names, imagespath, features):
        if features is not None:
            if self.addFaceFeatures(names, features):
                facedir = self.get_face_dir()
                return self.move_file_to_rootdir(facedir, names, imagespath)
        return False, None
