#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2017/8/2 11:03
@Author  : fuyu
@File    : nptool.py
@Software: IntelliJ IDEA
@Desc    : np的工具类
'''
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import numpy as np

import base64
def feature_to_b64encode(features):
    return base64.b64encode(toNpArray(features))
def prettyfloat(scores):
    return np.around(scores,decimals=3)

def getSize(data):
    if isinstance(data, np.ndarray):
        count=data.size
    elif isinstance(data,(str, list)):
        count=len(data)
    else:
        count=0
    return count


def toNpArray(data):
    if not isinstance(data, np.ndarray):
        np_array = np.array(data)
    else:
        np_array = data
    return np_array




def toArrayList(data):
    if isinstance(data, np.ndarray):
        data_list = data.tolist()
    else:
        data_list = data
    return data_list

def sort_corr(handle,feature, _keys, _features, count=100, score=0.6):
    fcount=getSize(_features)

    if feature is None and  fcount< 1:
        return None, None
    if fcount < count:
        count = fcount
    corr = np.dot(_features, feature)
    keys,scores=get_argsort(_keys,corr,count,score)
    if handle is not None:
        for i in xrange(count):
            handle(keys[i],"%.3f" % scores[i])
    else:
        return toArrayList(keys),toArrayList(scores)


def get_argsort(keys,scores,count=100,score=0.0000):
    _keys=toNpArray(keys)
    _scores=toNpArray(scores)
    vals_1 = np.arange(len(_keys))
    vals_index = vals_1[scores>score]
    _keys=_keys[vals_index]
    _scores=_scores[vals_index]
    indexs=np.argsort(toNpArray(_scores))
    _indexs=indexs[-count:][::-1]
    return _keys[_indexs],_scores[_indexs]


def max_corr(handle,feature, _keys, _features):
    if getSize(_features) > 0 and feature is not None:
        corr = np.dot(_features, feature.T)
        person_id = np.argmax(corr)
        confidence = np.amax(corr)
        key=_keys[person_id].replace("\n","")
        confidence="%.3f" % confidence
        if handle is not None:
            handle(key,confidence)
        return key,confidence
    return None, None

def get_max_corr(_features, features):
    corr =  np.dot(_features, features.T)
    confidence = np.amax(corr)
    return "%.3f" % confidence





def file_to_feature(content,version="1.4.2"):
    if content is None:
        return None
    size = len(content)
    if size == 0 or size % 2048 != 0:
        return None
    features=None
    try:
        features = np.fromstring(content, dtype=np.float32).reshape((-1,512))
        return features.shape[0],features
    except Exception,e:
        print("file_to_feature:{},{}".format(Exception,e))
    return -1,None




# [0 2 3 4 5] ['1' '3' '1' '3' '1']
def findDataIndexs(data,items):
    # data=["1","2","3","1","3","1"]
    # items = ["1","3"]
    np_array = toNpArray(data)
    np_array_reshape=np_array.reshape((-1, 1))
    item_index = np.where(np_array_reshape==toArrayList(items))
    indexs= item_index[0]
    values = findDataByIndexs(np_array,indexs)
    return indexs,values


def findDataByIndexs(data,indexs,pos=0):
    np_array = toNpArray(data)
    values = np_array[toNpArray(indexs)+pos]
    return values




# ['1' '3' '3' '1']
def deleteDataByIndexs(data,indexs):
    # data=["1","2","3","1","3","1"]
    # indexs = ["1","3"]
    return np.delete(toNpArray(data),toNpArray(indexs),axis=0)

def appendNpArray(data,item):
    features = np.vstack((toNpArray(data), toNpArray(item)))
    return features

def append(data,item):
    _data=toArrayList(data)
    if isinstance(item, list):
        _data.extend(item)
    else:
        _data.append(item)
    return _data
