#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2017/8/17 10:45
@Author  : fuyu
@File    : filetools.py
@Software: IntelliJ IDEA
@Desc    : 对特征值文件进行判断
'''
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import os
import copy
import nptools as fm


from ..path import loadFile, is_file_exists, read_lines,write_content,read_content

#必须
from ..path import clearFileOrDir,dirlist,mkdir


# 获取文件的内容
# features
def getfeature(featurepath):
    size, content = loadFile(featurepath)
    return toFeatures(size, content)


def toFeatures(size, content,version="1.4.2"):
    if size is not None and size > 0 and size % 2048 == 0:
        return fm.file_to_feature(content,version)
    return -1, None


# names
def getname(namepath):
    if is_file_exists(namepath):
        names = read_lines(namepath)
        return len(names), names
    return -1, None
#加载db
def loadDb(namepath,featurepath):
    nc,names=getname(namepath)
    fc,features=getfeature(featurepath)
    if nc>0 and nc==fc:
        return nc,names,features
    else:
        return -1,None,None
#追加db
def addDb(namepath, names,featurepath,features, overWrite="a+"):
    try:
        nc=len(names)
        fc=len(features)
        if names is not None and features is not None and nc == fc and nc>0:
            names_ = copy.copy(names)
            write_content("\n".join(fm.append(names_, "")), namepath, overWrite)
            write_content(fm.toNpArray(features).tostring(), featurepath, overWrite)
            return nc
    except Exception, e:
        print("FeatureThread add:{},{}".format(Exception, e))
    return 0






#获取文件的路径
def getface(dir):
    return os.path.join(dir, "face.jpg")
def getfeaturepath( dir):
    return os.path.join(dir, "features.bin")
def getnamepath( dir):
    return os.path.join(dir, "names.txt")


#----------------Angle--------------------
#angledir
def getangledir(dir, angle):
    return os.path.join(dir, "features_angle{}".format(angle))
#anglefeaturepath
def getanglefeaturepath(dir,angle):
    return getfeaturepath(getangledir(dir,angle))
#anglenamepath
def getanglenamepath(dir,angle):
    return getnamepath(getangledir(dir,angle))


def listangles(handle):
    for angle in xrange(1,28):
        if not handle(angle):
            break
def listangledir(dirpath,handle):
    for angle in xrange(1,28):
        angledir=getangledir(dirpath,angle)
        if not handle(angle,angledir):
            break