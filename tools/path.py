# encoding=utf-8
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import datetime
import os
import requests
import sys
import shutil
import time


def dirlist(path, filehandle, dirhandle):
    if is_file_exists(path):
        filehandle(path)
        return
    filelist = os.listdir(path)
    for filename in filelist:
        filepath = os.path.join(path, filename)
        if is_file_exists(filepath, 2):
            dirlist(filepath, filehandle, dirhandle)
        else:
            filehandle(filepath)
    dirhandle(path)


def cur_file_dir():
    # os.path.abspath(os.path.dirname(__file__))#获取当前目录
    # os.path.abspath(os.path.dirname(os.path.dirname(__file__)))#获取上级目录
    path = sys.path[0]
    if os.path.isdir(path):
        return path
    elif os.path.isfile(path):
        return os.path.dirname(path)


def loadFile(filepath):
    if not is_file_exists(filepath, 1):
        return None, None
    size = os.path.getsize(filepath)
    content = read_content(filepath)
    return size, content


def read_content(filepath):
    if not is_file_exists(filepath, 1):
        return None
    file_object = open(filepath)
    try:
        data = file_object.read()
    finally:
        file_object.close()
    return data


def write_content(content, filepath, flag="w"):
    mkdir(os.path.abspath(os.path.dirname(filepath)))
    with open(filepath, flag) as data:
        data.write(content)
        data.close()


def read_lines(filepath):
    data = None
    if not is_file_exists(filepath, 1):
        return None
    file_object = open(filepath)
    try:
        data = file_object.read().splitlines()
    finally:
        file_object.close()
    return data


def mkdir(parentdir, dirname=None):
    if dirname is None:
        dir = parentdir
    else:
        dir = os.path.join(parentdir, dirname)
    dir = os.path.abspath(dir)
    if not os.path.exists(dir):
        os.makedirs(dir)
    return dir


def mkfile(filepath):
    if not os.path.exists(filepath):
        with open(filepath, "w") as data:
            data.close()
    return filepath


def copyFileOrDir(oldfile, newfile, type=1):
    if not is_file_exists(oldfile, 3):
        return False
    if is_file_exists(newfile, 3):
        if type == 1:
            clearFileOrDir(newfile)
        elif type == 2:
            print "%s is exist" % newfile
            return False
        elif type == 3:  # 文件追加
            if is_file_exists(newfile, 1):
                content = read_content(oldfile)
                write_content(content, newfile, "a")
                return True
    try:
        # 复制文件
        mkdir(os.path.abspath(os.path.dirname(newfile)))
        shutil.copyfile(oldfile, newfile)
    except Exception as err:
        print(err)
        return False

    return True


def mvFileOrDir(oldfile, newfile, type=1):
    if not is_file_exists(oldfile):
        return False
    if is_file_exists(newfile):
        if type == 1:
            clearFileOrDir(newfile)
        elif type == 2:
            print "%s is exist" % newfile
            return False
        elif type == 3:  # 文件追加
            if is_file_exists(newfile, 1):
                content = read_content(oldfile)
                write_content(content, newfile, "a")
                clearFileOrDir(oldfile)
                return True
    try:
        mkdir(os.path.abspath(os.path.dirname(newfile)))
        shutil.move(oldfile, newfile)
    except Exception as err:
        print(err)
        return False
    return True


def clearFileOrDir(filepath):
    if is_file_exists(filepath, 1):
        try:
            os.remove(filepath)
        except:
            pass
    if is_file_exists(filepath, 2):
        try:
            shutil.rmtree(filepath)
        except:
            pass


def is_file_exists(filepath, type=1):
    if type == 1 and os.path.isfile(filepath) and os.path.exists(filepath):
        return True
    elif type == 2 and os.path.isdir(filepath) and os.path.exists(filepath):
        return True
    elif type == 3 and os.path.exists(filepath):
        return True
    return False


def uploadFileToLocal(url):
    filepath = None
    fileType = 0
    if "http://" in url and "https://" in url:
        file = requests.get(url)
        if file.status_code == 200:
            Suffix = os.path.splitext(url)[1]
            filepath = getfilepath(Suffix)
            write_content(file.content, filepath)
            fileType = 1
    elif os.path.exists(url):
        filepath = url
        fileType = 2

    if filepath is not None and filepath[0] is not "/":
        filepath = os.path.join(cur_file_dir(), filepath)

    return fileType, filepath


def getfilepath(Suffix, dir=None):
    if dir == None:
        dir = mkdir(os.path.abspath("."), "files")
    pathname = getfilename(Suffix)
    filepath = os.path.join(dir, pathname)
    return filepath


def getfilename(Suffix):
    timestr = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")
    filename = "{}{}".format(timestr, Suffix)
    return filename


def andomDirPath(dir):
    return os.path.join(dir, str(int(round(time.time() * 1000))))
