#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@Time    : 2017/8/1 12:32
@Author  : fuyu
@File    : http.py
@Software: IntelliJ IDEA
@Desc    : Function description
'''
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
import os

from path import getfilename,mkdir
from tool import getJsonVal
import requests

'''文件上传'''


def uploaded_file(data, key, filename=None, imagesdir="./data/images"):
    try:
        f = getJsonVal(data, key)
        if f is not None:
            Suffix = os.path.splitext(f.name)[1]
            if filename is None:
                filename = getfilename(Suffix)
            else:
                filename = "{}{}".format(filename, Suffix)
            mkdir(imagesdir)
            filepath = os.path.join(imagesdir, filename)
            destination = open(filepath, 'wb+')
            for chunk in f.chunks():
                destination.write(chunk)
            destination.close()
            return filepath
    except Exception, e:
        print e
    return None


def sendForm(url, form):
    ret = None
    try:
        ret = requests.post(url, data=form, timeout=10)
    except requests.exceptions.Timeout:
        print "sendForm:Timeout occurred"
    return ret


def sendFilesForm(url, form, imagepath):
    ret = None
    try:
        fa = open(imagepath, 'rb')
        multiple_files = {'image': fa}
        ret = requests.post(url, data=form, files=multiple_files)
        fa.close()
    except requests.exceptions.Timeout:
        print "sendFilesForm:Timeout occurred"
    return ret
