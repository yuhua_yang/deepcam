# -*- coding: utf-8 -*-
import multiprocessing
import os
import random
import time

from tools.dbUtils.mongo.mongodb_queue import MogoQueue
import tools.tf.genderDetect as gd

SLEEP_TIME = 1
from tools.tf.objectClassify import ObjectClassify

pbResourcesPath = os.path.join(os.path.dirname(os.path.abspath(__file__)), "resources/pbResources")
pbNames = ["gender", "Man-age", "Women-age"]
ROOT_DIR = "/"
def getrandint():
    randint = random.randint(-5, 5)
    if -5 <= randint < -3:
        randint = -5
    elif -3 <= randint < 2:
        randint = 0
    else:
        randint = 5
    return randint

def loadDetectSession(pbResourcesPath, pbNames):
    objectClassify = ObjectClassify()

    def get_label_lines(pbName):
        pb_path = os.path.join(pbResourcesPath, pbName)
        return objectClassify.get_label_lines(pb_path,
                                              pb_path)

    # pbName=["gender-optimized","Man-age-optimized","Women-age-optimized"]
    labels = map(get_label_lines, pbNames)
    objectClassify.initSession()
    return objectClassify, labels


def mzitu_crawler():
    face_queue = MogoQueue('deepcam', 'face')  ##这个是图片实际URL的队列

    objectClassify, labels = loadDetectSession(pbResourcesPath, pbNames)
    (genderLabel, manAgeLabel, womenAgeLabel) = labels

    while isRunning:
        while True:
            try:
                faceID = face_queue.pop()
            except KeyError:
                # print('队列没有数据')
                break
            else:
                start=time.time()
                face = face_queue.pop_id(faceID, key_lists=["url", "dbName", "imageID"])
                faceurl = face["url"]
                filepath = os.path.join(ROOT_DIR, faceurl)
                if os.path.exists(filepath):
                    graph = gd.load_graph('./tools/tf/constant_graph_weights.pb')
                    tensor = gd.read_tensor_from_image_file(filepath)

                    gender, genderscore = gd.detect(graph, tensor)
                    print('the gender is: ',gender)
                    faceData = objectClassify.readImage(image_path=filepath)
                    
                    if gender == "male":
                        age, agescore = objectClassify.detect(faceData, manAgeLabel)
                    else:
                        age, agescore = objectClassify.detect(faceData, womenAgeLabel)
                    print("start",gender, genderscore,age, agescore)
                    age = int(age.replace("age", "")) + getrandint()
                    face_queue.db.update({"_id": faceID}, {"$set": dict({"age": age, "sex": gender})})
                    if face.pop("dbName", None) == "vip":
                        face_queue.Client["features"].update({"_id": face.pop("imageID", None)},
                                                             {"$set": dict({"age": age, "sex": gender})})
                    print("end",gender, genderscore,age, agescore)
                print(time.time()-start)
                face_queue.complete(faceID)  ##设置为完成状态
                print('插入数据库成功')
        time.sleep(SLEEP_TIME)


def process_crawler():
    process = []
    num_cpus = int(multiprocessing.cpu_count() / 2)
    #num_cpus=1

    print('将会启动进程数为：', num_cpus)
    for i in range(num_cpus):
        p = multiprocessing.Process(target=mzitu_crawler, args=())  ##创建进程
        p.start()  ##启动进程
        process.append(p)  ##添加进进程队列
    for p in process:
        p.join()  ##等待进程队列里面的进程结束


if __name__ == "__main__":
    isRunning = True
    mzitu_crawler()
    try:
        isRunning = True
        process_crawler()
    except KeyboardInterrupt:
        isRunning = False
